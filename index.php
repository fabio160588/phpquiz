<?php declare(strict_types=1); ?>
<?php if (php_sapi_name() == "cli") :?>
<?php $rows = createTable(); ?>

        <?php 
        $maximumRangeX = 10;
        $mask = "|x |\n";
        for($a =5; $a<$maximumRangeX; $a++)
            {
            $mask = "|%5.5s".$mask;
            }
                foreach ($rows as $row) : ?>
                <?php ## printf ($mask,implode("|",$row)); ?>
                <?php printf ($mask,"1","1","1","1","1"); ?>
                <?php endforeach; ?>
<?php else: ?>

     <!DOCTYPE html>

    <head xmlns:color="http://www.w3.org/1999/xhtml">
        <title>Multiplication</title>
        <link rel="stylesheet" href="style.css">
        <style type="text/css">
        </style>


    </head>

    <body>

    <form action ='#' method="post" name="postData">

        X: <input name = "x" type="number" min = "1" max="25" required="true">
        Y: <input name = "y" type="number" min = "1" max="25" required="true">
        <input type="submit" value  ="Create">

    </form>
    <p></p>

    <table>
        <?php $rows = createTable(); ?>

        <?php foreach ($rows as $row) : ?>
            <tr>
                <td><?php echo implode("</td><td>", $row); ?></td>

            </tr>
        <?php endforeach; ?>
    </table>

    </body>

    </html>

<?php
endif;
?>
<?php


function createTable()
{
    if(isset($_POST['x']) && isset($_POST['y']))
    {

        $maximumRangeX = $_POST['x'];
        $maximumRangeY = $_POST['y'];
    }
    else
    {
        //Set some default values
     $maximumRangeX = 10;
        $maximumRangeY = 10;
    }
    $rows = array();
    ## Maximum range for time tables to go up to - configure variable here
   // $maximumRangeX = 22;
    //$maximumRangeY = 2;
    ##Create array with values from 1 to maximum configurable range
    $rows[0] = range(1, $maximumRangeX);

    ## Add X to the start of the Array to display on the table
    array_unshift($rows[0], "X");
    ## Begin Loop for the rows
    for ($a = 1; $a <= $maximumRangeY; $a++) {
        $rows[$a] = array($a);
        ## Begin loop for each column with in a row
        for ($b = 1; $b <= $maximumRangeX; $b++) {
            ## Performe calulcation and store data in 3 dimensional array
            $rows[$a][$b] = ($a * $b);
        }
    }
    #Return data
    return $rows;
}

?>